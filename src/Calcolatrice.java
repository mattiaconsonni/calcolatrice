import java.util.Scanner;

public class Calcolatrice {

    public static void main(String[] args) {
        System.out.println("Benvenuti allo strumento calcolatrice, inserire il tasto Q per spegnerla");
        Scanner in = new Scanner(System.in);
        String operator;
        while (true) {
            System.out.println("Inserire l'operando scelto: ");
            operator = in.nextLine();
            float operand1, operand2;
            operand1 = operand2 = 0;
            if (operator.equals("F"))
                break;
            System.out.println("Inserire il primo operando: ");
            operand1 = Float.parseFloat(in.nextLine());
            System.out.println("Inserire il secondo operando: ");
            operand2 = Float.parseFloat(in.nextLine());
            switch (operator) {
                case "+":
                    System.out.println("Il risultato è " + (operand1 + operand2));
                    break;
                case "-":
                    System.out.println("Il risultato è " + (operand1 - operand2));
                    break;
                case "*":
                    System.out.println("Il risultato è " + (operand1 * operand2));
                    break;
                case "/":
                    System.out.println("Il risultato è " + (operand1 / operand2));
                    break;
                default:
                    System.out.println("Operatore non esistente: riprovare");
            }
        }
        System.out.println("Calcolatrice spenta.");
    }
}
